import { Routes } from '@angular/router';
import { RootCompComponent } from './root-comp/root-comp.component';
import { UnlimitedScrollTableComponent } from './unlimited-scroll-table/unlimited-scroll-table.component';

export const appRoutes: Routes = [
  { path: 'home', component: RootCompComponent },
  { path: 'unlScroll', component: UnlimitedScrollTableComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];
