import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { UnlimitedScrollTableComponent } from './unlimited-scroll-table/unlimited-scroll-table.component';
import { RootCompComponent } from './root-comp/root-comp.component';


const routes: Routes = [

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
