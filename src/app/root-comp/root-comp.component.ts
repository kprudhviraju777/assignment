import { Component, OnInit } from '@angular/core';
import { AppServiceService } from '../services/app-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root-comp',
  templateUrl: './root-comp.component.html',
  styleUrls: ['./root-comp.component.css']
})
export class RootCompComponent implements OnInit {
  headers;
  data;
  totalRecords;

  constructor(private service: AppServiceService, private router: Router) {}

  ngOnInit() {
    this.headers = Object.keys(this.service.data[0]);
    const tempArr: any = [];
    this.totalRecords = this.service.data.length;
    this.service.data.forEach((X, index) => {
      if (index < 10) {
        tempArr.push(X);
      }
    });
    this.data = JSON.parse(JSON.stringify(tempArr));
  }

  pageSizeEvent(event) {
    const tempDataArr: any = [];
    this.service.data.forEach((X, index) => {
      if (event > index) {
        tempDataArr.push(X);
      }
    });
    this.data = JSON.parse(JSON.stringify(tempDataArr));
  }

  paginationChangeEvent(event) {
    console.log(event);
    const tempDataArr: any = [];
    const recordCount = (event.pageNum - 1) * event.pageSize;
    for (let i = recordCount; i < recordCount + event.pageSize; i++) {
      tempDataArr.push(this.service.data[i]);
    }
    this.data = JSON.parse(JSON.stringify(tempDataArr));
  }

  onSubmition(event) {
    console.log(event);
    const body: any = {
      id: event.id,
      status: event.status
    };
    this.service.submitRowData(body).subscribe(
      X => {
        console.log(X);
      },
      error => {
        console.log(error);
      }
    );
  }
  onNavigation() {
    this.router.navigate(['/unlScroll']);
  }
}
