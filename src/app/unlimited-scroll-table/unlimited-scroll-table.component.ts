import { Component, OnInit, Input } from '@angular/core';
import { AppServiceService } from '../services/app-service.service';

@Component({
  selector: 'app-unlimited-scroll-table',
  templateUrl: './unlimited-scroll-table.component.html',
  styleUrls: ['./unlimited-scroll-table.component.css']
})
export class UnlimitedScrollTableComponent implements OnInit {
  @Input() headers: any;
  @Input() data: any;

  constructor(private service: AppServiceService) {}

  ngOnInit() {
    this.headers = Object.keys(this.service.data[0]);
    this.data = JSON.parse(JSON.stringify(this.service.data));
  }
}
