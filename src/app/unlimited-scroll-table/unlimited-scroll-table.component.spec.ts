import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnlimitedScrollTableComponent } from './unlimited-scroll-table.component';

describe('UnlimitedScrollTableComponent', () => {
  let component: UnlimitedScrollTableComponent;
  let fixture: ComponentFixture<UnlimitedScrollTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnlimitedScrollTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnlimitedScrollTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
