import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.css']
})
export class DatatableComponent implements OnInit {
  @Input() headers: any;
  @Input() data: any;
  @Input() totalRecords: any;

  @Output() pageSizeEvent = new EventEmitter<any>();
  @Output() paginationChangeEvent = new EventEmitter<any>();
  @Output() onSubmition = new EventEmitter<any>();

  pageSize: any = 10;
  pageIndex = 1;
  paginationLength;
  paginateArr: any = [];

  constructor() {}

  ngOnInit() {
    this.generatePaginationControls();
  }
  generatePaginationControls() {
    const div = this.totalRecords / this.pageSize;
    const mod = this.totalRecords % this.pageSize;
    if (mod !== 0) {
      const tempNum = Math.round(div);
      for (let i = 1; i <= div; i++) {
        this.paginateArr.push(i);
      }
    } else {
      for (let i = 1; i <= div; i++) {
        this.paginateArr.push(i);
      }
    }
    console.log(this.paginateArr);
  }

  onPageSizeChanged(event) {
    this.paginateArr = [];
    this.pageIndex = 1;
    this.pageSizeEvent.emit(parseInt(event));
    this.generatePaginationControls();
  }

  onPageChange(pageNum, pageSize) {
    this.paginateArr = [];
    this.pageIndex = pageNum;
    this.paginationChangeEvent.emit({ pageNum, pageSize });
    this.generatePaginationControls();
  }
  onPaginatorChange(event) {
    if (event === 1) {
      this.onPageChange(this.pageIndex - 1, this.pageSize);
    } else {
      this.onPageChange(this.pageIndex + 1, this.pageSize);
    }
  }

  onRowSubmit(event) {
    this.onSubmition.emit(event);
  }
}
